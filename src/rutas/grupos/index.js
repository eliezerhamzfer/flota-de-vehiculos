module.exports = app => {
  const Grupos = app.configDB.modelos.Grupos;

  app.get('/grupos/', (req, res) => {
    Grupos.findAll()
    .then(result => res.json(result))
    .catch(error => {
      res.status(412).json({msg: error.message});
    });
  });

  app.get('/grupos/:idgrupo', (req, res) => {
    Grupos.findById(req.params.idgrupo)
    .then(result => res.json(result))
    .catch(error => {
      res.status(412).json({msg: error.message});
    });
  });

  app.delete('/grupos/:idgrupo', (req, res) => {
    Grupos.destroy({where: {id_grupo: req.params.idgrupo}})
      .then(result => res.sendStatus(204))
      .catch(error => {
        res.status(412).json({msg: error.message});
      });
  });

  app.post('/grupos', (req, res) => {
    Grupos.create(req.body)
      .then(result => res.json(result))
      .catch(error => {
        res.status(412).json({msg: error.message});
      });
  });

};