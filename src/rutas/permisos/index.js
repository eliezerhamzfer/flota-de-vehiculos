module.exports = app => {
    const Permisos = new Object();
    Permisos.nombreRuta = 'Permisos';
    Permisos.modelos = app.configDB.modelos.Permisos;

    app.get('/permisos/', (req, res) => {
      Permisos.modelos.findAll()
      .then(result => res.json(result))
      .catch(error => {
        res.status(412).json({msg: error.message});
      });
    });
  
    app.get('/permisos/:idpermiso', (req, res) => {
      Permisos.modelos.findById(req.params.idgrupo)
      .then(result => res.json(result))
      .catch(error => {
        res.status(412).json({msg: error.message});
      });
    });
  
    app.delete('/permisos/:idpermiso', (req, res) => {
      Permisos.modelos.destroy({where: {id_grupo: req.params.idgrupo}})
        .then(result => res.sendStatus(204))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
    });
  
    app.post('/Permisos', (req, res) => {
      Permisos.modelos.create(req.body)
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({msg: error.message});
        });
    });
    return Permisos
  };