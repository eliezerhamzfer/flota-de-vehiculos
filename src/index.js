// Babel nos permite transformar JS de última generación a JS que cualquier navegador o versión de Node.js entienda
// importamos modulo express
// consign usamos para estructurar el proyecto
import express from 'express';
import consign from 'consign';



const app = express();

//ejecutamos consign, en este caso va a leer desde el directorio src
// con cwd le decimos el directorio
// include lo primero que queremos ejecutar
consign({
    cwd: __dirname
})
.include('libs/configuracionDatabase.js')
.then('configDB.js')
.then('../app.js')
.then('./passport/autenticacionLocal.js')
.then('libs/middlewares/index.js')
.then('rutas')
.then('libs/boot-server.js')
.into(app)

 