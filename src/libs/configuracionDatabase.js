// exportamos un objeto

module.exports = {
    database: 'flotadevehiculos',
    username: '',
    password: '',
    params: {
        dialect: 'sqlite',
        storage: 'flota.sqlite',
        define: {
            underscore: true
        },
        operatorsAliases: false
    }
};