import Sequelize from 'sequelize';
import fs from 'fs';
import path from 'path';

//almacenamos la ruta de modelos
const dir = path.join(__dirname, 'modelos');
let subFolders = fs.readdirSync(dir).sort();
let db = null;

// exportamos ua configuracion de la base de datos
module.exports = function (app) {
    // accedemos a archivos con consign de la sigte. manera
    const configDB = app.libs.configuracionDatabase
    // si no existe una db, estblecemos una conexion nueva
    if(!db){
        // almaceno la conexion incializada
        const sequelize = new Sequelize(
            configDB.database,
            configDB.username,
            configDB.password,
            configDB.params
        );
        //ya incializamos la base de datos
        // ahora exportamos la conexion BD, el modulo, los modelos que seran un objeto vacio
        db = {
            sequelize,
            Sequelize,
            modelos: {}
        }
        //recorremos cada subcarpeta del directorio './modelos/'
        subFolders.forEach(carpeta => {
            let dirSubCarpeta = path.join(dir, carpeta + '/');
            // ahora accedemos a las subcarpeta de modelos, la sigte funcion nos las devuelve en un arreglo
            fs.readdirSync(dirSubCarpeta).forEach(filename => {
                //unimos el directorio con el nombre del archivo que nos devuelve
                const directorioModelos = path.join(dirSubCarpeta, filename);
                //importamos cada archivo, y sequelize lo puede ejecutar, y sequelize nos va a devolver un modelo de dato
                const modelo = sequelize.import(directorioModelos);
                //entonces desde mi objeto db, guardamos una propiedad en el objeto modelos
                db.modelos[modelo.name] = modelo;
            });
        });
        //leemos los archivos con "Object.keys", y le pedimos la clave de los modelos, y ejecutamos sus relaciones (manytomany, etc)
        Object.keys(db.modelos).forEach(key => {
            if (db.modelos[key].associate) {
                db.modelos[key].associate(db.modelos);
            }
        }); 
        //leemos los archivos con "Object.keys", y le pedimos la clave de los modelos, y ejecutamos sus relaciones (manytomany, etc)
        Object.keys(db.modelos).forEach(key => {
            if (db.modelos[key].associate) {
                db.modelos[key].associate(db.modelos);
            }
        }); 
    }

    // si existe le devolvemos db
    return db;
};