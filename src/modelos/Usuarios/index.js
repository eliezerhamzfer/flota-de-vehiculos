module.exports = (sequelize, DataType) => {
	const Usuario = sequelize.define('Usuario', {
		id_usuario: {
			type: DataType.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		username: {
			type: DataType.STRING(32),
			unique: true,
      		allowNull: false,
			validate: {
				notEmpty: true
			}
        },
        first_name: {
			type: DataType.STRING(32),
			unique: false,
      		allowNull: true,
			validate: {
				isAlpha: true
			}
		},
        last_name: {
			type: DataType.STRING(32),
			unique: false,
      		allowNull: true,
			validate: {
				isAlpha: true
			}
		},
		email: {
			type: DataType.STRING,
			unique: true,
			allowNull: false,
			validate: {
                notEmpty: true,
                isEmail: true,
                notNull: true
			}
		},
		password: {
			type: DataType.STRING,
			allowNull: false,
			validate: {
                notEmpty: true,
                notNull: true
			}
        },
        is_staff: {
            type: DataType.BOOLEAN,
        },
        is_active: {
            type: DataType.BOOLEAN,
        },
        is_superuser: {
            type: DataType.BOOLEAN,
        },
        last_login: {
            type: DataType.DATE
        }
	});

	Usuario.associate = (models) => {
        Usuario.belongsToMany(models.Grupos, {
            through: 'GrupoUsuarios',
            as: 'grupos',
            foreignKey: 'usuario_id'
        });
    };

    Usuario.associate = (models) => {
        Usuario.belongsToMany(models.Permisos, {
            through: 'PermisosUsuarios',
            as: {
				singular: 'permiso', 
				plural: 'permisos'
			},
            foreignKey: 'usuario_id'
        });
    };

	return Usuario;

};