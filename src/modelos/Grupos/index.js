module.exports = (sequelize, DataType) => {
	const Grupos = sequelize.define('Grupos', {
		id_grupo: {
			type: DataType.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		nombre_grupo: {
			type: DataType.STRING(80),
			unique: true,
      		allowNull: false,
			validate: {
				notEmpty: true
			}
        }
	});

	Grupos.associate = (models) => {
        Grupos.hasMany(models.Usuario);
    };

	return Grupos;

};