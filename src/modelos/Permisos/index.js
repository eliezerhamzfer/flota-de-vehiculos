module.exports = (sequelize, DataType) => {
	const slugify = function (text){
		return text.toString().toLowerCase()
		.replace(/\s+/g, '-')           // Reemplaza espacios con  -
		.replace(/[^\w\-]+/g, '')       // Remueve todo lo que no sean letras
		.replace(/\-\-+/g, '-')         // Reemplaza varios -- con uno simple -
		.replace(/^-+/, '')             // Recorta - al inicio del texto
		.replace(/-+$/, '');            // Recorta - al final del texto
	}
	const Permisos = sequelize.define('Permisos', {
		id_permiso: {
			type: DataType.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		nombre_permiso: {
			type: DataType.STRING(100),
			unique: true,
      		allowNull: false,
			validate: {
				notEmpty: true
			}
		},
        slug: {
        	type: DataType.STRING(100),
            allowNull: false,
          	validate: {
            	notEmpty: true
          	}
        }
	},{
		hooks: {
			beforeCreate: function (instance) {
				instance.set('slug', slugify(instance.get('nombre_permiso')));
			}
		}
	});

	Permisos.associate = (models) => {
        Permisos.hasMany(models.Usuario);
    };

	return Permisos;

};