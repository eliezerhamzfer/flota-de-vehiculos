import passport from 'passport';
import session from 'express-session';
import morgan from 'morgan';

module.exports = function (app){
    app.use(session({
        secret: '5emZKMYUB9C2vT6',
        resave: false,
        saveUninitialized: false
    }));
    app.use(morgan('dev'));
    app.use(passport.initialize());
    app.use(passport.session());    
    app.set('port', process.env.PORT || 3000);

    // Configurar cabeceras y cors
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

        next();
    });

}
